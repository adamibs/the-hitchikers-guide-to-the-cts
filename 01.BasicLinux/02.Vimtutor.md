
### Goals
- The trainee will understand how to work with Vim editor

### Tasks
- Answer the following questions:
  - Explain the 3 modes in `vim`


**Command Mode**
While `vim` is waiting for input

**Insert Mode**
While typing input is appended at cursor point.

**Visual Mode**
While cursor may be used to move around the file and performing operations on the file such as copying and deleting are acceptable.


  - How can you delete an entire line of text?
```sh
:dd
```
  - How can you delete 4 lines of text?
```sh
:4dd
```

  - How can you paste some text?
```sh
:p
```

  - How can you upward-search for the work "splendid"?
```sh
:?splendid
```
  - How can you change all the occurences of the word "leet" to the word "1337"?
```sh
:%s/leet/1337/
```
