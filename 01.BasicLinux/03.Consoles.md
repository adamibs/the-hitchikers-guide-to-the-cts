
### Goals
- The trainee will understand the different types of consules in a linux environment
- The trainee will know how to check who is connected to a machine

### Tasks
- Read about the following:
  - Physical TTY
  - Local Pseudo TTY
  - Remote Pseudo TTY
  - `tty`
  - `who`
- Explain the difference between Physical, Local Pseudo and Remote Pseudo TTY.

| Name          | Description                                                                                                            | Example Use Case                 |
|---------------|------------------------------------------------------------------------------------------------------------------------|----------------------------------|
| Physical      | Source of text based I/O, while physically connected to the instance.                                                  | Console of a bare metal instance |
| Local Pseudo  | Functions as a physical terminal as a wrapper providing similar functionality                                          | Windows CMD, tmux                |
| Remote Pseudo | Functions as a bidirectional connection to a remote instance with functionality similar to that of a physical terminal | SSH, ILO                         |

- Run the tty command and explain it’s output
- Answer the following questions:
  - SSH is example to which kind of TTY?
  Remote Pseudo
  - How many physical consoles exist in a Linux environment?
  1
  - What is the meaning of the number of files in the /dev/pts directory?
- Login to your second physical tty, run the `who` command and exaplain the meaning of the output
